#!/usr/bin/env python
import sys
import math
#import numpy

threshold=0.7

clusterCentroids=[]
#clusters=[]
def tokenize_phrase(phrase):
  #data=phrase.replace("'", " ")
  data=phrase.split()
  return data

def levenshtein(s1, s2):
    if len(s1) < len(s2):
        return levenshtein(s2, s1)
    if not s1:
        return len(s2)
 
    previous_row = xrange(len(s2) + 1)
    for i, c1 in enumerate(s1):
        current_row = [i + 1]
        for j, c2 in enumerate(s2):
            insertions = previous_row[j + 1] + 1 # j+1 instead of j since previous_row and current_row are one character longer
            deletions = current_row[j] + 1       # than s2
            substitutions = previous_row[j] + (c1 != c2)
            current_row.append(min(insertions, deletions, substitutions))
        previous_row = current_row
 
    return previous_row[-1]


  

def getCosSim(p1, p2):
  global DF, N

  words1=tokenize_phrase(p1)
  words2=tokenize_phrase(p2)
  #print words1, words2

  vector=set(words1) | set(words2)

  inner=0.0
  sqsum1=0.0
  sqsum2=0.0
  for word in vector:
    idf=math.log(float(N)/DF[word])
    tfidf1=words1.count(word)*idf
    tfidf2=words2.count(word)*idf
    inner+=tfidf1*tfidf2
    sqsum1+=tfidf1**2
    sqsum2+=tfidf2**2

  cosine=inner/(math.sqrt(sqsum1) * math.sqrt(sqsum2))

  #print cosine

  return cosine
  """
  # Jaccard's
  if len(set1)<2 or len(set2)<2:
    return 0
  try:
    return float(len(set1 & set2)) / len(set1 | set2)
  except:
    return 0
  """


def getSim(p1, p2):
  words1=tokenize_phrase(p1)
  words2=tokenize_phrase(p2)
  dist=levenshtein(words1, words2)
  denom=max(len(words1), len(words2))

  LevSim=1.0-(dist/float(denom))

  CosSim=getCosSim(p1, p2)

  if CosSim>0.9 and 0.3*LevSim+0.7*CosSim>0.5:
    sys.stderr.write("!!!!!!! %s\t%s\t%f\t%f\n" % (words1, words2,LevSim,  CosSim))

  return CosSim

def delStopwords(wordlist=[]):
  stopwords=set(["a","about","above","after","again","against","all","also","am","an","another","and","any","are","aren't","as","at","be","because","been","before","being","below","between","both","but","by","can't","cannot","could","couldn't","did","didn't","do","does","doesn't","doing","don't","down","during","each","few","for","from","further","had","hadn't","has","hasn't","have","haven't","having","he","he'd","he'll","he's","her","here","here's","hers","herself","him","himself","his","how","how's","i","i'd","i'll","i'm","i've","if","in","into","is","isn't","it","it's","its","itself","let's","me","more","most","mustn't","my","myself","no","nor","not","of","off","on","once","only","or","other","ought","our","ours ","ourselves","already","out","over","own","same","shan't","she","she'd","she'll","she's","should","shouldn't","so","some","such","than","that","that's","the","their","theirs","them","themselves","then","there","there's","these","they","they'd","they'll","they're","they've","this","those","through","to","too","under","until","up","very","was","wasn't","we","we'd","we'll","we're","we've","were","weren't","what","what's","when","when's","where","where's","which","while","who","who's","whom","why","why's","with","won't","would","wouldn't","you","you'd","you'll","you're","you've","your","yours","yourself","yourselves", "january", "february", "march", "april", "may", "june", "july", "august", "september", "october", "november", "december", "jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec", "'", "", " ", "ll", "t", "d", "ve", "re", "s", "m", "(...)"] )

  stopcnt=0.0
  outwordlist=[]
  for word in wordlist:
    #if word.isdigit():
    #  outwordlist.append("numeric")
    #else:
    if word not in stopwords:
      outwordlist.append(word)
    
  return outwordlist


meme_index={}
meme_list=[]
#meme_set_list=[]
meme_to_id={}


line_no=0
for line in open(sys.argv[1]):
  line_no+=1
  if line_no%1000==0:
    sys.stderr.write(str(line_no)+"\n")


  meme=line.strip()

  data=tokenize_phrase(meme)
  #data=delStopwords(data)

  meme_list.append(meme)
  #meme_set_list.append(set(data))

  cur_meme_id=len(meme_list)-1

  meme_to_id[meme]=cur_meme_id

  for word in data:
    try:
      meme_index[word].add(cur_meme_id)
    except:
      meme_index[word]=set([cur_meme_id])

DF={}
for word in meme_index.keys():
  DF[word]=len(meme_index[word])
N=len(meme_list)
#print DF

sys.stderr.write("Done Reading the file. Emitting Pairs\n")

#Now map strips
emit_dic={}
line_no=0
candi_size=[]

for line in sys.stdin:
  
  line_no+=1
  if line_no%100==0:
    sys.stderr.write(str(line_no)+"\n")

  meme=line.strip()
  data=tokenize_phrase(meme)
  #data=delStopwords(data)
  meme_set=set(data)

  meme_id=meme_to_id[meme]

  candi_list=set()
  for word in meme_set:
    if DF[word]/float(N)>0.01:
      #sys.stderr.write(str(word)+"\n")
      continue
    candi_list=candi_list | meme_index[word]

  #candi_size.append(len(candi_list))
  #continue

  inCluster=0
  for candi in candi_list:
    try:
      if meme_id in emit_dic[candi]:
        continue
    except:
      pass

    try:
      if candi in emit_dic[meme_id]:
        continue
    except:
      pass


    if meme_id==candi:
      continue

    set1=meme_list[meme_id]
    set2=meme_list[candi]
    sim=getCosSim(set1, set2)
    
    if sim>threshold:
      print str(sim)+"\t"+meme_list[meme_id]+"\t"+meme_list[candi]
      inCluster=1
    try:
      emit_dic[meme_id].add(candi)
    except:
      emit_dic[meme_id]=set([candi])

  if inCluster==0:
    print "0.0\t"+meme_list[meme_id]

#sys.stderr.write("Average candidate size: %.2f\n" % (numpy.average(candi_size)) )
#sys.stderr.write("Average candidate sigma: %.2f\n" % (numpy.std(candi_size)) )
#sys.stderr.write("Min candidate: %d\n" % (max(candi_size)) )
#sys.stderr.write("Max candidate: %d\n" % (min(candi_size)) )

