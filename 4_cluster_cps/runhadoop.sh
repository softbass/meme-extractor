output=memes_cluster
input_file=top.meme.list
splitted_dir=top.meme.splitted

echo "getting top meme list"
cat ../3_rank_cps/daily_top_memes/* | python get_meme_list.py > $input_file
rm -rf $splitted_dir
mkdir $splitted_dir
split -l2000 $input_file $splitted_dir/

hadoop fs -rmr $splitted_dir 
hadoop fs -put $splitted_dir $splitted_dir

echo "Done! Running Hadoop"

hadoop fs -rmr $output

hadoop jar $HADOOP_HOME/contrib/streaming/hadoop-0.20.2-streaming.jar \
  -D mapred.output.key.comparator.class=org.apache.hadoop.mapred.lib.KeyFieldBasedComparator \
  -D stream.num.map.output.key.fields=1 \
  -D mapred.text.key.comparator.options="-k1nr"\
  -input $splitted_dir -output $output \
  -file mapper.py -mapper "mapper.py $input_file"\
  -file reducer.py -reducer reducer.py \
  -file $input_file\
  -numReduceTasks 1

rm -rf $output
hadoop fs -get $output $output

