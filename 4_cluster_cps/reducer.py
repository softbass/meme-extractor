#!/usr/bin/env python
import sys

def getSim(set1, set2):
  try:
    return float(len(set1 & set2)) / len(set1 | set2)
  except:
    return 0

cluster_set_list=[]
cluster_dic={}

step_no=0
for line in sys.stdin:
  step_no+=1
  data=line.strip().split("\t")
  cluster_keys=set(cluster_dic.keys())
  if data[0]=="0.0":
    #sys.stderr.write(data[1]+"\n") 
    cluster_set_list.append(set([ data[1] ]))
    continue

  new_cluster1=-1
  new_cluster2=-1
  if data[1] in cluster_keys:
    new_cluster1=cluster_dic[data[1]]
  if data[2] in cluster_keys:
    new_cluster2=cluster_dic[data[2]]  

  if new_cluster1==-1 and new_cluster2==-1 :
    cluster_set_list.append(set([data[1], data[2]]))
    cluster_dic[ data[1] ]=len(cluster_set_list)-1
    cluster_dic[ data[2] ]=len(cluster_set_list)-1
  elif new_cluster1==-1 and new_cluster2>-1 :
    cluster_set_list[new_cluster2].add(data[1])
    cluster_dic[ data[1] ]=new_cluster2
  elif new_cluster1>-1 and new_cluster2==-1 :
    cluster_set_list[new_cluster1].add(data[2])
    cluster_dic[ data[2] ]=new_cluster1
  elif new_cluster1==new_cluster2:
    #already in the same cluster
    pass
  else:
    for meme in cluster_set_list[new_cluster2]:
      cluster_dic[ meme ]=new_cluster1

    cluster_set_list[new_cluster1] = cluster_set_list[new_cluster1] | cluster_set_list[new_cluster2] 
    cluster_set_list[new_cluster2] = []
  
  #print cluster_dic
i=0
for cluster in cluster_set_list:
  if len(cluster)>0:
    i+=1
    print str(i)+"\t"+",".join(cluster)





