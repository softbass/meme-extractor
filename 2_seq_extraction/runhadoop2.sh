output=blog_memes_2

hadoop fs -rmr $output

hadoop jar $HADOOP_HOME/contrib/streaming/hadoop-0.20.2-streaming.jar \
  -D mapred.output.key.comparator.class=org.apache.hadoop.mapred.lib.KeyFieldBasedComparator \
  -D stream.num.map.output.key.fields=2 \
  -D num.key.fields.for.partition=1 \
  -D mapred.text.key.comparator.options="-k1,1 -k2,2n"\
  -input blog_memes_1 -output $output \
  -file mapper2.py -mapper mapper2.py \
  -file reducer2.py -reducer reducer2.py \
  -partitioner org.apache.hadoop.mapred.lib.KeyFieldBasedPartitioner \
  -numReduceTasks 1024


#   -D stream.map.output.field.separator=\t \
#  -D map.output.key.field.separator=\t \

rm -rf $output
hadoop fs -get $output $output

