#!/usr/bin/env python

#Mapper 1
import sys
import operator
import re

ngram=5

def replace_html_entries(sentence):
  #replacemap={"&quot;": "\"", "&apos;": "'", "&lt;": "<", "&gt;": ">", "&amp;":"&", "&nbsp;":" ","' ":" ", " '":" ", "\"":"'" }
  replacemap={"&quot;": "\"", "&apos;": "'", "&lt;": "<", "&gt;": ">", "&amp;":"&", "&nbsp;":" ","' ":" ", " '":" ", "\"":"'" }

  for k,v in replacemap.items():
    sentence=sentence.replace(k,v)

  sentence=re.sub("\'(?!\w)+|(?<!\w)\'", "", sentence)  
        
  return sentence
  
for line in sys.stdin:
  data=line.strip().split("\t")
  if len(data)!=3:
    continue
  title=data[0]
  text=data[2]
  overall=0

  text=text.lower()

  text=replace_html_entries(text)
  
  words=re.findall("[\w\']+", text)
  
#  words=re.findall("[\w\&][\w\&\']*[\w\;]", text)
  i=0
  for word in words:
    if i>=ngram-1:
      ngram_list= words[i-(ngram-1):i+1]
      print " ".join(ngram_list)+"\t"+title+","+str(overall-(ngram-1))
    i+=1
    overall+=1

