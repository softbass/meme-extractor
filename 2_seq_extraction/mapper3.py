#!/usr/bin/env python

#Extender

import sys
import operator
import re

ngram=5

def addCombiner(meme, docID):
  global combiner, cnt
  try:
    combiner[meme].append(docID)
  except:
    combiner[meme]=[docID]
  cnt+=1  
  #print "%s\t%s" % (meme,docID) 
  

def flush():
  #return
  global cnt, combiner
  sys.stderr.write(str(cnt)+" flushed\n")
  for k,v in combiner.items():
    print "%s\t%s" % (k,v) 
  cnt=0
  combiner={}
      
prev_doc=""
combiner={}
cnt=0

seqDoc={}
for line in sys.stdin:
  #print line.strip()
  data=line.strip().split("\t")
  
  docID=data[0]
  seq=int(data[1])
  length=int(data[2])
  meme=data[3]


  #Filter out shingles
  initWord=meme.split()[0]
  #print initWord
  if initWord.isdigit():
    continue
    
  cflag=False
  for stopword in ["comments on this post", "date", "posted", "a response or", "links to weblogs", "www", "com","srch", "permalink", "january", "february", "march", "april", "may", "june", "july", "august", "september", "october", "november", "december", "published online", "mailto", "align center", "align left", "align right", "times new roman", "quizilla", "s m t w t f s", "ahref", "create a link","0922 x8857 to get started","the end of the day", "the first time", "i love youi love you", "&gt;", "&lt;", "jan", "feb", "mar", "apr","may","jun","jul","aug","sep","oct","nov","dec", "url", "trackback", "feed", "rss", "date of","http","postimees", "2008", "2009", "2007", "2006", "2005", "utc", "add to memories","tell friend","current mood", "weblogs that reference this entry", "free online", "link reply parent thread", "previous entry", "next entry", "pcetlsbizwdp","bibhzcb0ywcgls0", "view all", "related related searches", "usability stability installation functionality", "cjxzy3jpchqg","bgfuz3vhz2u9ikp","hdmfty3jpchqii","hr5cgu9inrlehqvam","f2yxnjcmlwdci", "cjxzy3jpchqgbg","fuz3vhz2u9ikphdm","fty3jpchqiihr5cgu9inrleh","qvamf2yxnjcmlwdci", "in print or online","must be granted by the author"]:
    if stopword in meme:
      cflag=True
      break
  if cflag:
    continue
  #print data
  #print "%s\t%s" % (meme,docID) 
  
  addCombiner(meme, docID)
      
  curpos=seq+length
  
  if (prev_doc!=docID):
    seqDoc={}
  
  temp_dic_add={}
  temp_dic_del=set()
  for pos in seqDoc:
    if seq-pos<=ngram*2 and seq-pos>ngram :
      #print seqDoc[pos]+" (...) "+meme+"\t"+docID
      addCombiner(seqDoc[pos]+" (...) "+meme, docID)
      #print seqDoc[pos]+" (...) "+meme, docID
      temp_dic_add[curpos]=seqDoc[pos]+" (...) "+meme
      temp_dic_del.add(pos)
      break
    if seq-pos>ngram*2 :
      temp_dic_del.add(pos)
  
  for pos in temp_dic_del:
    del seqDoc[pos]
  for pos in temp_dic_add:
    seqDoc[pos]=temp_dic_add[pos]

  seqDoc[seq+length]=meme
  
  
  prev_doc=docID
  
  
  if cnt>100000:
    flush()
  
flush()



