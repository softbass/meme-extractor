#!/usr/bin/env python

#Mapper 2

import sys
import operator
import re

prev_line=""
for line in sys.stdin:
  data=line.strip().split("\t")
  #print data

  meme=data[0]

  occrs=data[1].split("|")

  for occr in occrs:
    docid,index=occr.split(",")

    if len(occrs)>100:
      output_line=docid+"\t"+index+"\t"+meme+"\tfreepass\t-1"

      print output_line
      #prev_line=output_line
      continue
      
    for occr2 in occrs:
      docid2,index2=occr2.split(",")
      if docid!=docid2:
        output_line=docid+"\t"+index+"\t"+meme+"\t"+occr2.replace(",", "\t")
        print output_line


