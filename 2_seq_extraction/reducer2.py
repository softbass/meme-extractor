#!/usr/bin/env python

#Reducer 2

import sys
import operator
import re

prev_doc_id=""
src_seq=[]

#doc id, index
#src_seq[]
activeSequences=[]
#sequences=set()

def emitActiveSeq(prev_doc_id, activeSequences):
  seqset=set()
  for seq in activeSequences:
    seqset.add( (seq["pos"],seq["len"],seq["memes"]) )

  for seq in sorted(list(seqset)):
    print "%s\t%d\t%d\t%s" % (prev_doc_id, seq[0], seq[1],seq[2])

for line in sys.stdin:
  data=line.strip().split("\t")
  #print data
  #print activeSequences
  src_doc_id = data[0]
  src_index = eval(data[1])
  meme_text = data[2]
  trg_doc_id = data[3]
  trg_index = eval(data[4])

  if prev_doc_id!=src_doc_id or src_index-prev_src_index>1:
    emitActiveSeq(prev_doc_id, activeSequences)
    activeSequences=[]
    #sequences=set()

  delete_freepasses=[]
  for seq in activeSequences:
    #Current sequence started from a freepass
    if seq["tar_docID"]=="freepass":
      if trg_doc_id=="freepass":
        if seq["pos"]+seq["len"]==src_index:
          seq["len"]+=1
          seq["memes"]+=" "+meme_text.split()[-1]
        #else:
        #  print "%s\t%s" % (src_doc_id, str(seq)) 
        #  activeSequences.remove(seq)
      else:
        if seq["pos"]+seq["len"]==src_index:
          newseq=seq.copy()
          newseq["tar_docID"]=trg_doc_id
          newseq["tar_pos"]=trg_index-seq["len"]
          #newseq["len"]+=1
          #this will be iterated again, so don't add "len"
          activeSequences.append(newseq)
          if seq not in delete_freepasses:
            delete_freepasses.append(seq)
          #print activeSequences
    else:
      if trg_doc_id=="freepass":
        if seq["pos"]+seq["len"]==src_index:
          seq["len"]+=1
          seq["memes"]+=" "+meme_text.split()[-1]
        #else:
        #  print "%s\t%s" % (src_doc_id, str(seq)) 
        #  activeSequences.remove(seq)
      else:
        nextPos=seq["tar_pos"]+seq["len"]
        nextShingle=(seq["tar_docID"], nextPos)
        if (seq["tar_docID"]==trg_doc_id):
          if nextShingle == (trg_doc_id, trg_index):
            seq["len"]+=1
            seq["memes"]+=" "+meme_text.split()[-1]
          else:
            #if there are many shingles from one target doc
            if seq["len"]>src_index-seq["pos"]:
              continue
            #else:
            #  print "%s\t%s" % (src_doc_id, str(seq)) 
            #  activeSequences.remove(seq)
      
  #delete starting free passes that followed by nonfreepass
  for seq in delete_freepasses:
    activeSequences.remove(seq)

  #Ignore shingles from the same doc: I don't think so!
  if src_doc_id != trg_doc_id:
  #if True:
  #Freepass can start a sequence only for the source doc
  #if src_doc_id != trg_doc_id or (src_doc_id == trg_doc_id and trg_index==src_index):
    if trg_doc_id not in (x["tar_docID"] for x in activeSequences):
      newSeq={}
      newSeq["pos"]=src_index
      newSeq["tar_docID"]=trg_doc_id
      newSeq["tar_pos"]=trg_index
      newSeq["len"]=1
      newSeq["memes"]=meme_text
      activeSequences.append(newSeq)
    
  prev_doc_id = src_doc_id
  prev_src_index=src_index

#if prev_doc_id!=src_doc_id:# and len(sequences)>0:
emitActiveSeq(prev_doc_id, activeSequences)


