#!/usr/bin/env python

#Reducer 3

import sys
import operator
import re

source_dic={}
sys.stderr.write("reading doc-source.table\n")
for line in open("doc-source.table"):
  data=line.strip().split("\t")
  source_dic[data[0]]=data[1]

prev_phrase=""
output=[]
outputdates=set()

def flush(prev_phrase, output, outputdates):
  joinedout=[doc+"|"+source_dic[doc] for doc in output]
  if len(joinedout)>=3:
    print "%s\t%s" % (prev_phrase, ",".join(joinedout))

sys.stderr.write("done!\n")
for line in sys.stdin:
  data=line.strip().split("\t")
  phrase=data[0]
  docs=eval(data[1])

  if prev_phrase!=phrase and len(output)>0:
    flush(prev_phrase, output, outputdates)
    output=[]
    outputdates=set()
  
  output.extend(docs)
  for doc in docs:
    outputdates.add(doc[7:15])
  
  prev_phrase=phrase


flush(prev_phrase, output, outputdates)
