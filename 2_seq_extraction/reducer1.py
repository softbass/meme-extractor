#!/usr/bin/env python

#Reducer1

import sys
import operator
import re

lower_bound=5
upper_bound=225000

def printOutput(prev_phrase, output):
  doc_set=set([x.split(",")[0] for x in output])
  #print doc_set
  if len(doc_set)>=lower_bound and len(doc_set)<=upper_bound:
    print prev_phrase+"\t"+"|".join(output)


prev_phrase=""
output=[]
cnt=0

skip_flag=False
for line in sys.stdin:
  data=line.strip().split("\t")
  phrase=data[0]

  if prev_phrase!=phrase:
    if len(output)>0:
      if skip_flag==False:
        printOutput(prev_phrase,output)
    skip_flag=False
    output=[]
    cnt=0

    
  if skip_flag==False:
    output.append(data[1])

  prev_phrase=phrase
  cnt+=1
  if cnt>upper_bound:
    output=[]
    skip_flag=True

if skip_flag==False:
  printOutput(prev_phrase,output)
