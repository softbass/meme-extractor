#!/usr/bin/python

import sys

for line in sys.stdin: #input is /scratch/0003/dicta19/blogs08/data/2_5gram/meme_cluster_src.table
  data=line.strip().split("\t")
  meme=data[0]

  #Delete trashes heuristically
  if meme.startswith("says"):
    continue

  #print meme
  initWord=meme.split()[0]
  #print initWord
  if initWord.isdigit():
    continue

  cflag=False
  for stopword in ["comments on this post", "date", "posted", "a response or", "links to weblogs", "www", "com","srch", "permalink", "january", "february", "march", "april", "may", "june", "july", "august", "september", "october", "november", "december", "published online", "mailto", "align center", "align left", "align right", "times new roman", "quizilla", "s m t w t f s", "ahref", "create a link","0922 x8857 to get started","the end of the day", "the first time", "i love youi love you", "&gt;", "&lt;", "jan", "feb", "mar", "apr","may","jun","jul","aug","sep","oct","nov","dec", "url", "trackback", "feed", "rss", "date of","http","postimees", "2008", "2009", "2007", "utc", "add to memories","tell friend","current mood", "weblogs that reference this entry", "free online", "link reply parent thread", "previous entry", "next entry", "pcetlsbizwdp","bibhzcb0ywcgls0", "view all", "related related searches", "usability stability installation functionality", "cjxzy3jpchqg","bgfuz3vhz2u9ikp","hdmfty3jpchqii","hr5cgu9inrlehqvam","f2yxnjcmlwdci", "cjxzy3jpchqgbg","fuz3vhz2u9ikphdm","fty3jpchqiihr5cgu9inrleh","qvamf2yxnjcmlwdci", "in print or online","must be granted by the author"]:
    if stopword in meme:
      cflag=True
      break
      

      
  if cflag:
    continue  

  docs=data[1].split(",")
  #print docs
  dates=[]
  srcs={}
  uniqdocs=set()
  for d in docs:
    docid, source=d.split("|")
    uniqdocs.add(docid)
    #source=".".join(source.split(".")[-2:])
    #print source
    date=docid[7:15]
    dates.append(date)
    try:
      srcs[date].add(source)
    except:
      srcs[date]=set([source])
  dates=[d[7:15] for d in uniqdocs]
  uniq_dates=set(dates)
  
  #DF=float(len(uniq_dates))
  DF=float(len(dates))
  for d in uniq_dates:
    outputval=((len(srcs[d])-1)*dates.count(d))/DF #number of uniq docs on that day/number of all uniq docs
    if outputval>0.3:
      print "%s\t%s\t%.3f" % (d,meme, outputval) #+ " %d,%d,%f" %(len(srcs[d]), dates.count(d), DF)
