#!/usr/bin/python

import sys
import operator
curdate=""
meme_dic={}

def cleanup(curdate, meme_dic):
	cut=100
	sorted_x = sorted(meme_dic.iteritems(), key=operator.itemgetter(1), reverse=True)
	top_memes=[x[0] for x in sorted_x[:cut]]
	print "%s\t%s" % (curdate, ",".join(top_memes))
	return

for line in sys.stdin: 
	data=line.strip().split("\t")
	date=data[0]

	if curdate!=date and curdate!="":
		cleanup(curdate, meme_dic)
		meme_dic={}

	meme_dic[data[1]]=float(data[2])
	
	curdate=date

cleanup(curdate, meme_dic)
