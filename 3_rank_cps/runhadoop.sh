output=daily_top_memes

hadoop fs -rmr $output

hadoop jar $HADOOP_HOME/contrib/streaming/hadoop-0.20.2-streaming.jar \
  -input blog_memes_3 -output $output \
  -file mapper.py -mapper mapper.py \
  -file reducer.py -reducer reducer.py \
  -numReduceTasks 86

rm -rf $output
hadoop fs -get $output $output

