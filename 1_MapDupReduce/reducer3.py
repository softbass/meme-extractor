#!/usr/bin/env python

#Reducer 2

import sys
import operator
import re


def output(docid,cntdic):
  tau_doc=0.65
  #print docid,cntdic
  doc1=docid.split(",")
  for d2, cnt in cntdic.items():
    doc2=d2.split(",")
    
    #print docid, cntdic  
    sim=float(cnt)/float(int(doc1[1])+int(doc2[1])-cnt)
    if sim>tau_doc:
      #print doc1, doc2, cnt
      print doc1[0]+"\t"+doc2[0]+"\t"+str(sim)#, int(doc1[2]), doc2[2], cnt

prev_phrase=""
cntdic={}

for line in sys.stdin:
  data=line.strip().split("\t")
  
  phrase=data[0]
  dupcount=eval(data[1])

  if prev_phrase!=phrase and len(cntdic)>0:
    output (prev_phrase, cntdic)

    cntdic={}
  
  for k, v in dupcount.items():
    try:
      cntdic[k]+=v
    except:
      cntdic[k]=v

  prev_phrase=phrase


output (prev_phrase, cntdic)
