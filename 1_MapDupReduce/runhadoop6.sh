output=blogs08_dedup_6

hadoop fs -rmr $output

hadoop jar $HADOOP_HOME/contrib/streaming/hadoop-0.20.2-streaming.jar \
  -input undedup_stripped -output $output \
  -file mapper6.py -mapper mapper6.py \
  -file deletion.list -file sorted_duplicate_to_delete.list \
  -numReduceTasks 500

rm -rf $output
hadoop fs -get $output $output


