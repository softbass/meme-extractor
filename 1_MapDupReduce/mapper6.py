#!/usr/bin/env python

#Mapper 6: Omitting without ones on the deletion list
import sys
import operator
import re

docs_to_del=set()

for line in open("sorted_duplicate_to_delete.list"):
  docs_to_del.add(line.strip())

for line in open("deletion.list"):
  docs_to_del.add(line.strip())

sys.stderr.write(str(len(docs_to_del))+" docs to delete\n")

for line in sys.stdin:
  data=line.strip().split("\t")
  if len(data)!=3:
    continue
  
  docid=data[0]
  if docid in docs_to_del:
    #sys.stderr.write(line)
    pass
  else:
    print line.strip()
    

  
