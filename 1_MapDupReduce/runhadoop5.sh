output=blogs08_dedup_5

hadoop fs -rmr $output

hadoop fs -put duplicate_to_delete.list duplicate_to_delete.list

hadoop jar $HADOOP_HOME/contrib/streaming/hadoop-0.20.2-streaming.jar \
  -input duplicate_to_delete.list -output $output \
  -file mapper5.py -mapper mapper5.py \
  -numReduceTasks 10

rm -rf $output
hadoop fs -get $output $output

sort -m blogs08_dedup_5/p* | uniq > sorted_duplicate_to_delete.list
