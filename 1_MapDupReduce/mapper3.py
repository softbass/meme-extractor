#!/usr/bin/env python

#Mapper 2

import sys
import operator
import re

tau=0.65 #jaccard sim between sentences, I gave it marginally

def getSim(d1,d2):
  #input: doc id, sentence id, num of s in d (doc len), len of s (s len)
  l1=int(d1.split(",")[1])
  l2=int(d2.split(",")[1])
  #ds1_str=",".join(ds1_data[:3])
  
  sim=min(l1,l2)/float(max(l1,l2))
  #print l1,l2,sim
  return sim
    
prev_phrase=""
doc_list=[]

pair_count={}

linecount=0
paircnt=0
def flushdic(pair_count):
  for k,v in pair_count.items():
    print k+"\t"+str(v)

for line in sys.stdin:
  data=line.strip().split("\t")

  #print data

  phrase=data[0]
  doc_list=data[1].split("|")
  
  i=0
  doc_list=sorted(doc_list)
  
  for i, doc2 in enumerate(doc_list):
    for doc1 in doc_list[:i]:
      #if doc1==doc2:
      #  continue
      sim=getSim(doc1, doc2)
      if (sim>=tau ):
        paircnt+=1
        if paircnt>100000:
          sys.stderr.write(str(linecount)+" line: "+str(i)+"/"+str(len(doc_list))+" docs\n")
          flushdic(pair_count)
          pair_count={}
          paircnt=0
    
        try:
          pair_count[doc1][doc2]+=1
        except:
          try:
            pair_count[doc1][doc2]=1
          except:
            pair_count[doc1]={doc2:1}
          
          
    i+=1

  #To avoid timeout and memory explosion
  linecount+=1


flushdic(pair_count)  
sys.stderr.write("total "+str(linecount)+" lines\n") 

