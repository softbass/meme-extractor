#!/usr/bin/env python

#Reducer1

import sys
import operator
import re

lower_bound=1
upper_bound=5000

prev_phrase=""
output=[]
cnt=0

skip_flag=False
for line in sys.stdin:
  data=line.strip().split("\t")
  phrase=data[0]

  if prev_phrase!=phrase:
    if cnt>upper_bound:
      print prev_phrase
    cnt=0

  prev_phrase=phrase
  cnt+=1


if cnt>upper_bound:
  print prev_phrase


