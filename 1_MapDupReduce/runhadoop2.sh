output=blogs08_dedup_2

hadoop fs -rmr $output

hadoop jar $HADOOP_HOME/contrib/streaming/hadoop-0.20.2-streaming.jar \
  -input undedup_stripped  -output $output \
  -file blacksigs.list \
  -file mapper2.py -mapper mapper2.py \
  -file reducer2.py -reducer reducer2.py \
  -numReduceTasks 470

rm -rf $output
hadoop fs -get $output $output

