#!/usr/bin/env python

#Mapper 1
import sys
import operator
import re

ngram=5

stopwords=set(["a","about","above","according","across","after","afterwards","again","against","albeit","all","almost","alone","along","already","also","although","always","am","among","amongst","an","and","another","any","anybody","anyhow","anyone","anything","anyway","anywhere","apart","are","around","as","at","av","be","became","because","become","becomes","becoming","been","before","beforehand","behind","being","below","beside","besides","between","beyond","both","but","by","can","cannot","canst","certain","cf","choose","contrariwise","cos","could","cu","day","do","does","doesn't","doing","dost","doth","double","down","dual","during","each","either","else","elsewhere","enough","et","etc","even","ever","every","everybody","everyone","everything","everywhere","except","excepted","excepting","exception","exclude","excluding","exclusive","far","farther","farthest","few","ff","first","for","formerly","forth","forward","from","front","further","furthermore","furthest","get","go","had","halves","hardly","has","hast","hath","have","he","hence","henceforth","her","here","hereabouts","hereafter","hereby","herein","hereto","hereupon","hers","herself","him","himself","hindmost","his","hither","hitherto","how","however","howsoever","i","ie","if","in","inasmuch","inc","include","included","including","indeed","indoors","inside","insomuch","instead","into","inward","inwards","is","it","its","itself","just","kind","kg","km","last","latter","latterly","less","lest","let","like","little","ltd","many","may","maybe","me","meantime","meanwhile","might","moreover","most","mostly","more","mr","mrs","ms","much","must","my","myself","namely","need","neither","never","nevertheless","next","no","nobody","none","nonetheless","noone","nope","nor","not","nothing","notwithstanding","now","nowadays","nowhere","of","off","often","ok","on","once","one","only","onto","or","other","others","otherwise","ought","our","ours","ourselves","out","outside","over","own","per","perhaps","plenty","provide","quite","rather","really","round","said","sake","same","sang","save","saw","see","seeing","seem","seemed","seeming","seems","seen","seldom","selves","sent","several","shalt","she","should","shown","sideways","since","slept","slew","slung","slunk","smote","so","some","somebody","somehow","someone","something","sometime","sometimes","somewhat","somewhere","spake","spat","spoke","spoken","sprang","sprung","stave","staves","still","such","supposing","than","that","the","thee","their","them","themselves","then","thence","thenceforth","there","thereabout","thereabouts","thereafter","thereby","therefore","therein","thereof","thereon","thereto","thereupon","these","they","this","those","thou","though","thrice","through","throughout","thru","thus","thy","thyself","till","to","together","too","toward","towards","ugh","unable","under","underneath","unless","unlike","until","up","upon","upward","upwards","us","use","used","using","very","via","vs","want","was","we","week","well","were","what","whatever","whatsoever","when","whence","whenever","whensoever","where","whereabouts","whereafter","whereas","whereat","whereby","wherefore","wherefrom","wherein","whereinto","whereof","whereon","wheresoever","whereto","whereunto","whereupon","wherever","wherewith","whether","whew","which","whichever","whichsoever","while","whilst","whither","who","whoa","whoever","whole","whom","whomever","whomsoever","whose","whosoever","why","will","wilt","with","within","without","worse","worst","would","wow","ye","yet","year","yippee","you","your","yours","yourself","yourselves"])

def replace_html_entries(sentence):
  replacemap={"&quot;": "\"", "&apos;": "'", "&lt;": "<", "&gt;": ">", "&amp;":"&", "&nbsp;":" ","' ":" ", " '":" ", "\"":"'" }

  for k,v in replacemap.items():
    sentence=sentence.replace(k,v)

  sentence=re.sub("\'(?!\w)+|(?<!\w)\'", "", sentence)  
        
  return sentence

spotSigs=[]
spotSigsOut=set()

deletion_spotsig_lowerbound=1

for line in sys.stdin:
  data=line.strip().split("\t")
  if len(data)!=3:
    continue
  docid=data[0]
  text=data[2].lower()
  overall=0
  
  #if len(data[2])<20:
  #  f=open("too_short", "a")
  #  f.write(title+"\n")
  #  continue
  

  text=replace_html_entries(text)
  
  words=re.findall("[\w\']+", text)
  
  i=0
  #print text
  for word in words:
    if len(spotSigsOut)>deletion_spotsig_lowerbound:
      break
      
    if word in stopwords:
      spotSigs.append([word])
    else:
      for sig in spotSigs:
        sig.append(word)
        if len(sig)==ngram+1:
          spotSigsOut.add(":".join(sig))
          spotSigs.remove(sig)

  if len(spotSigsOut)<=deletion_spotsig_lowerbound:
    print docid

  spotSigs=[]
  spotSigsOut=set()

#print " ".join(ngram_list)+"\t"+docid+","+str(len(words)-3)#str(overall-(ngram-1))
