output=blogs08_dedup_3

hadoop fs -rmr $output

hadoop jar $HADOOP_HOME/contrib/streaming/hadoop-0.20.2-streaming.jar \
  -input blogs08_dedup_2 -output $output \
  -file mapper3.py -mapper mapper3.py \
  -file reducer3.py -reducer reducer3.py \
  -numReduceTasks 300

#  -file reducer3.py -reducer reducer3.py \

rm -rf $output
hadoop fs -get $output $output

cat blogs08_dedup_3/* | cut -f2 > duplicate_to_delete.list
