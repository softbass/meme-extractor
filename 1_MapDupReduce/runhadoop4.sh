output=blogs08_dedup_4

hadoop fs -rmr $output

hadoop jar $HADOOP_HOME/contrib/streaming/hadoop-0.20.2-streaming.jar \
  -input undedup_stripped -output $output \
  -file mapper4.py -mapper mapper4.py \
  -numReduceTasks 110

rm -rf $output
hadoop fs -get $output $output
sort -m $output/part* > deletion.list

