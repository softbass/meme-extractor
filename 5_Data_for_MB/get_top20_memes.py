#!/usr/bin/python
import sys
import commands 
import os

cluster_dic={}
cluster_id_to_cps={}
for line in open("../4_cluster_cps/memes_cluster/part-00000"):
  data= line.strip().split("\t")
  meme_id=int(data[0])
  meme_cps=data[1].split(",")
    
  cluster_id_to_cps[meme_id]=data[1]
  
  for cp in meme_cps:
    cluster_dic[cp]=meme_id

top_meme_dic={}

#Get the doc list
cp_set=set(cluster_dic.keys())

cp_dic={}
for line in open("meme_doc_src.list"):
  data=line.strip().split("\t")
  cp=data[0]
  docs=data[1]
  cp_dic[cp]=docs


for line in open("daily_top_memes.data"):
  data=line.strip().split("\t")
  date=data[0]
  cps=data[1].split(",")[:100]
  top_memes=[]

  for cp in cps:
    if cluster_dic[cp] not in top_memes:
      top_memes.append(cluster_dic[cp])
      if len(top_memes)==20:
        break
  top_meme_dic[date]=top_memes
 
days=sorted(top_meme_dic.keys())

doc_set=set()
cp_set=set()
meme_set=set()

print "output/daily_top_memes.list"
f=open("output/daily_top_memes.list", "w")

for day in days:
  for i, meme_id in enumerate(top_meme_dic[day]):
    f.write("%s\t%d\t%d\n" % (day, i+1, meme_id))
    cp_set = cp_set | set( cluster_id_to_cps[meme_id].split(",") )
    meme_set.add(meme_id)
f.close()

print "output/memes.list"
f=open("output/memes.list", "w")
for meme_id in sorted(meme_set):
  f.write("%d\t%s\n" % (meme_id, cluster_id_to_cps[meme_id] ))
f.close()

print "output/cp-doc-source.list"
f=open("output/cp-doc-source.list", "w")
for cp_id in sorted(cp_set):
  f.write("%s\t%s\n" % (cp_id, cp_dic[cp_id] ))
  for x in cp_dic[cp_id].split(","):
    doc_set.add(x.split("|")[0])
f.close()

print "output/docs.set"      
f=open("output/docs.set", "w")
f.write(str(doc_set))
f.close()


  
  
  
