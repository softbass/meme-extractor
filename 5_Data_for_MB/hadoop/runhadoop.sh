output=top_docs

hadoop fs -rmr $output
cp ../output/docs.set ./

hadoop jar $HADOOP_HOME/contrib/streaming/hadoop-0.20.2-streaming.jar \
  -input undedup_html -output $output \
  -file mapper.py -mapper mapper.py \
  -file docs.set\
  -numReduceTasks 80

rm -rf $output
hadoop fs -get $output $output

