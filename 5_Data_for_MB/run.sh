#Prepare for get_top_20_memes

mkdir output
rm output/*

echo "Getting the meme-doc-src list"
cat ../2_seq_extraction/blog_memes_3/* | python doc_filter.py > meme_doc_src.list

echo "Getting top-ranked memes"
sort -m ../3_rank_cps/daily_top_memes/part-000* > daily_top_memes.data

echo "Joining the data"
./get_top20_memes.py


cd hadoop
cp ../output/docs.set ./
echo "Run runlocal.sh or runhadoop.sh to get the html documents!"
rm -rf docs
mkdir docs
./runlocal_threads.sh

cat docs/* > ../output/top_docs.data
cd ..
cd output
rm memes.tar.gz
tar czvf memes.tar.gz *

