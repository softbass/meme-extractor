#!/usr/bin/python
import sys
import commands 
import os

cluster_dic={}
cluster_id_to_cps={}
for line in open("../4_cluster_cps/memes_cluster/part-00000"):
  data= line.strip().split("\t")
  meme_id=int(data[0])
  meme_cps=data[1].split(",")
    
  cluster_id_to_cps[meme_id]=data[1]
  
  for cp in meme_cps:
    cluster_dic[cp]=meme_id

top_meme_dic={}

#Get the doc list
cp_set=set(cluster_dic.keys())

cp_dic={}
for line in sys.stdin:
  data=line.strip().split("\t")
  cp=data[0]
  docs=data[1]
  if cp in cp_set:
    print line.strip()
